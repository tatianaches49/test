﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Pet
    {
        string name;
        int age;
        string Lastname;
        public Pet(string name, int age, string name2) {
            this.age = age;
            this.name = name;
            this.Lastname = name2;
        }
        public  string ToString()
        {
            return $"{age}:{name}:{Lastname}";
        }
    }
}
