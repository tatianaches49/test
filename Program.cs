﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        delegate int GetValue();
        static void Main(string[] args)
        {
            var v1 = new GetValue(Value1);
            var v2 = new GetValue(Value2);
            var chain = v1;
            chain += v2;
            Console.WriteLine(chain());
        }
        static int Value1() { return 1; }
        static int Value2() { return 2; }
    }
}
